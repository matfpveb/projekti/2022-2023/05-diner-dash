require('dotenv').config()

const path = require('path')
const express = require('express')
const mongoose =  require('mongoose')
const {urlencoded, json} = require('body-parser')
const fileUpload = require('express-fileupload')
const cors = require('cors')
const router = require('./routes/router.js')

const authMiddleware = require('./middlewares/authMiddleware.js')
const requestValidationMiddleware = require('./middlewares/requestValidationMiddleware')

const userRouter = require('./routes/api/userRouter')
const authRouter = require('./routes/api/authRouter')
const categoryRouter = require('./routes/api/categoryRouter.js')
const commentRouter = require('./routes/api/commentRouter.js')
const orderRouter = require('./routes/api/orderRouter.js')
const dishRouter = require('./routes/api/dishRouter.js')


const app = express()

let url = ''

if(process.env.MONGODB_CLUSTER_URL)
    url = process.env.MONGODB_CLUSTER_URL
else
    url = `mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`


console.log(url)
mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.on('error', (error) => {
    console.log(error + "\nEnding..")
    return
})

app.use(express.static(path.join(__dirname, 'public')))

app.set('views', 'views')

app.use(cors())
app.use(fileUpload())
app.use(json())
app.use(urlencoded({ extended: false }))

app.use('/auth/', authRouter)

app.use(authMiddleware)
app.use(requestValidationMiddleware)

app.use('/api/users', userRouter)
app.use('/api/categories', categoryRouter)
app.use('/api/comments', commentRouter)
app.use('/api/orders', orderRouter)
app.use('/api/dishes', dishRouter)

app.use(function (req, res, next) {
    res.status(404).sendFile(path.join(__dirname, 'views', '404.html'))
});

module.exports = app
