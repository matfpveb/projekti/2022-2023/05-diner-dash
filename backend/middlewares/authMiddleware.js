const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
    const headers = req.headers

    try {
        const token = headers.authorization.split(' ')[1]
        const decoded = jwt.verify(token, process.env.JWT_SECRET)

        if(decoded)
        {
            req.user = decoded
            next()
            return
        }

        throw new Error("Unauthorized")
    }
    catch (error){
        return res.status(401).json({ message: error.message})
    }
}