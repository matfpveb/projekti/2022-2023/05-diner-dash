module.exports = (req, res, next) => {
    const { username, password, email } = req.body
    if(!username && !email) return res.status(400).json({ error: 'username or email required' })

    if(!password) return res.status(400).json({ error: 'password required' })

    next()
}