const mongoose = require('mongoose')
const User = require('../models/userModel')

function getMissingRequiredFields(body) {
    const {username, email, password, first_name, last_name} = body
    const requiredFields = []

    if(!username) requiredFields.push('username')
    if(!email) requiredFields.push('email')
    if(!password) requiredFields.push('password')
    if(!first_name) requiredFields.push('first_name')
    if(!last_name) requiredFields.push('last_name')

    return requiredFields
}


module.exports = async (req, res, next) => {
    const missingRequiredFileds = getMissingRequiredFields(req.body)
    if(missingRequiredFileds.length) 
        return res.status(400).json({ error: "Required fields missing", requiredFields: missingRequiredFileds })

    const body = req.body

    const user = await User.findOne({ $or: [{username: body.username}, {email: body.email}] }).lean().exec()

    if(user) return res.status(400).json({ error: "User already exists" })

    next()
}