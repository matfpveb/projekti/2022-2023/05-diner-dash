class RequestValidator {
    constructor(fields, model) {
        this.fields = fields
        this.model = require('../models/' + model + "Model")
        this.valid = true
    }

    validate(method) {
        switch(method) {
            case "GET": 
            case "PATCH": return
            case "PUT": {
                this.validatePUTFields() 
                return
            }
            default: this.validateFields()
        }
    }

    validateFields() {
        this.missingFields = []
        const requiredSchemaFields = this.model.schema.requiredPaths()

        for( let key of requiredSchemaFields)
        {
            let index = this.fields.indexOf(key)
            if(index == -1) 
                this.missingFields.push(key + " is required")
        }
        this.valid = !this.missingFields.length

        if(!this.valid)
            this.error = { message: "Missing required fields", missingFields: this.missingFields }
    }

    validatePUTFields()
    {
        const availableFields = Object.keys(this.model.schema.paths)
        this.fields.forEach(( key, i ) => {
            const fieldIndex = availableFields.indexOf(key)
            if(fieldIndex == -1)
                this.fields.splice(i, 1)
        })

        this.valid = this.fields.length
        if(!this.fields.length) 
            this.error = { message: "There are no valid fields for PUT method" } 
    }

    isValid() {
        return this.valid
    }

    getMissingFields()
    {
        return this.missingFields
    }

    getError(){
        return this.error
    }
}

function parseModelString(model) {
    if(model.endsWith('ies')) {
        return model.slice(0, -3) + "y"
    }

    if(model.endsWith('es')) {
        return model.slice(0, -2)
    }

    return model.slice(0, -1)
}

module.exports = (req, res, next) => {

    const fields = req.body ? Object.keys(req.body) : null
    let model = req.url.split('/')[2]

    model = parseModelString(model)

    const validator = new RequestValidator(fields, model)
    
    validator.validate(req.method)

    if(!validator.isValid()) {
        res.status(400).json({ error: validator.getError() })
        return
    }

    next()
}

