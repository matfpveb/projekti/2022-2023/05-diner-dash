const express = require('express');

const controller = require('../controllers/controller.js');
const router = express.Router();

router.get('/', controller.home);
router.get('/jelovnik', controller.jelovnik);
router.get('/dostava', controller.dostava);
router.get('/kontakt', controller.kontakt);
router.get('/galerija', controller.galerija);

module.exports = router;