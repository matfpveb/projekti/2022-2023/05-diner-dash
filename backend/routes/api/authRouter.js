const express        = require('express')
const router         = express.Router()
const authController = require('../../controllers/authController') 

const loginMiddleware = require('../../middlewares/loginMiddleware')
const userCreateMiddleware = require('../../middlewares/userCreateMiddleware')

router.post('/login', loginMiddleware, authController.login)
router.post('/register', userCreateMiddleware, authController.register)

module.exports = router