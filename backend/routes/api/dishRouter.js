const express         = require('express')
const router          = express.Router()
const dishController = require('../../controllers/dishController') 

router.get('/', dishController.index)
router.post('/', dishController.store)
router.get('/:id', dishController.show)
router.put('/:id', dishController.update)
router.delete('/:id', dishController.deleteDish)

module.exports = router
