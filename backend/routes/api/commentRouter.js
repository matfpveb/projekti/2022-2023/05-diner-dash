const express         = require('express')
const router          = express.Router()
const commentController = require('../../controllers/commentController') 

router.get('/', commentController.index)
router.post('/', commentController.store)
router.get('/:id', commentController.show)
router.put('/:id', commentController.update)
router.delete('/:id', commentController.deleteComment)

module.exports = router
