const express         = require('express')
const router          = express.Router()
const usersController = require('../../controllers/usersController') 

const userCreateMiddleware = require('../../middlewares/userCreateMiddleware')

router.get('/', usersController.index)
router.post('/', usersController.store)
router.get('/:id', usersController.show)
router.put('/:id', usersController.update)
router.delete('/:id', usersController.deleteUser)

module.exports = router