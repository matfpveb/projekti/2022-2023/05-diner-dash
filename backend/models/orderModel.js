const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    total_price : {
        type: Number,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    floor: {
        type: Number
    },
    intercom_number: {
        type: Number
    },
    chef_message: {
        Type: String
    },
    delivery_price: {
        type: Number,
        required: true
    },
    order_price: {
        type: Number,
        required:true
    },
    order_status: {
        type: Number,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    items: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Dish',
        required: true
    }]
})

module.exports = mongoose.model("Order", orderSchema)
