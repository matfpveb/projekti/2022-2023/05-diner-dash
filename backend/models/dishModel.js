const mongoose = require('mongoose')

const dishSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    category_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: false
    },
    price: {
        type: Number,
        required: true
    }
})

module.exports = mongoose.model("Dish", dishSchema)
