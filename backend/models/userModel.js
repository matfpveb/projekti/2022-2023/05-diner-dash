const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    first_name: {
        type: String,
        required: false
    },
    last_name: {
        type: String,
        required: false
    },
    is_admin: {
        type: Boolean        
    },
    is_delivery: {
        type: Boolean
    },
    is_chef: {
        type: Boolean
    },
    image_path: {
        type: String,
        required: false
    }
})

module.exports = mongoose.model("User", userSchema)