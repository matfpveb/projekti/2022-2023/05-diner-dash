const http = require("http");
const app = require('./app');
const server = http.createServer(app);

let port = 8080;
server.listen(port);

server.once('listening', function () {
    console.info(`Pokrenut server http://localhost:${port}`);
});
