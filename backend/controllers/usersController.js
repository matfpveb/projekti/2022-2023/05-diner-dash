const mongoose = require('mongoose')
const { uploadImage } = require('./apiController')
const bcrypt = require('bcrypt')
const User = require('../models/userModel')

async function index(req, res, next) {
    User.find({})
        .exec()
        .then((users) => {
            res.status(200).json({
                userCount: users.length,
                users: users
            })
        })
        .catch((error) => {
            res.status(500).json({
                error: error
            })
        })
        console.log(req.protocol)
}

async function show(req, res, next) {
    const id = req.params.id
    User.findById(id)
        .then((user) => {
            res.status(200).json({
                user: user
            })
        })
        .catch((error) => {
            res.status(500).json({
                error: error
            })
        })
}

async function store(req, res, next) {
    const body = req.body
    bcrypt.hash(body.password, parseInt(process.env.BCRYPT_ROUNDS), (err, hash) => {
        if(err) {
            return res.status(500).json({ error: "Hashing failed" })
        }
        
        body.password = hash
        const user = new User({ _id: mongoose.Types.ObjectId(), ...body })
        user.save()
            .then((user) => {
                return res.status(201).json({
                    message: "User created",
                    user: user
                })
            })
            .catch((error) => {
                return res.status(500).json({
                    error: error
                })
            })

    })
    
}

async function update(req, res, next) {
    const id = req.params.id
    const body = req.body

    if(req.files) {
        const imageName = await uploadImage(Object.values(req.files)[0])
        body.image_path = req.protocol + "://" + req.get('host') + "/" + imageName
    }

    bcrypt.hash(body.password, parseInt(process.env.BCRYPT_ROUNDS), (err, hash) => {
        if(err) {
            return res.status(500).json({ error: "Hashing failed" })
        }

        body.password = hash
        User.findByIdAndUpdate(id, body, ( err, user ) => {
            if(err){
                res.status(500).json({
                    error: err
                })
                return
            }
    
            res.status(200).json({
                message: "User successfully updated",
                user: user 
            })
        })
    })
}

async function deleteUser(req, res, next) {
    const id = req.params.id
    User.findById(id)
    .then((user) => {
        user.delete()
            .then( (result) => {
                res.status(200).json({
                    message: "User successfully deleted",
                    user:result
                })
            })
            .catch( (error) => {
                res.status(500).json({
                    error: error
                })
            })
    })
    .catch( () => {
        res.status(404).json({
            error: "User not found"
        })
    })
}

module.exports = {
    index,
    show,
    store,
    update,
    deleteUser
}
