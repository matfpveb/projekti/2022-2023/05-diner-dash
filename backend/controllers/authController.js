const mongoose = require('mongoose')
const User = require('../models/userModel')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

async function login(req, res, next) {
    const body = req.body
    User.findOne().or([ {username: body.username}, {email: body.email} ])
        .exec()
        .then(( user ) => {
            bcrypt.compare(body.password, user.password, ( error, hash ) => {
                    if(error) {
                        return res.status(401).json({
                            message: error
                        })
                    }

                    const token = jwt.sign({
                        _id: user._id,
                        username: user.username,
                        email: user.email,
                        first_name: user.first_name,
                        last_name: user.last_name,
                        is_admin: user.is_admin,
                        is_delivery: user.is_delivery,
                        is_chef: user.is_chef,
                        image_path: user.image_path
                    }, process.env.JWT_SECRET, { expiresIn: '1d' })

                    return res.status(200).json({
                        messsage: "Login successful",
                        token: token
                    })
                })
        })
        .catch(( error ) => {
            return res.status(200).json({ error: "User not found" })
        })
}

async function register(req, res, next) {
    //TODO validation
    const body = req.body
    bcrypt.hash(body.password, parseInt(process.env.BCRYPT_ROUNDS), (err, hash) => {
        if(err) {
            return res.status(500).json({ error: err })
        }
        user = new User({
            _id: mongoose.Types.ObjectId(),
            username: body.username,
            password: hash,
            email: body.email,
            first_name: body.first_name,
            last_name: body.last_name
        })
        user.save()
            .then((user) => {
                const token = jwt.sign({
                    _id: user._id,
                    username: user.username,
                    email: user.email,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    is_admin: user.is_admin,
                    is_delivery: user.is_delivery,
                    is_chef: user.is_chef,
                    image_path: user.image_path
                }, process.env.JWT_SECRET, { expiresIn: '1d' })

                return res.status(201).json({ message: "User created", token: token })
            })
            .catch( (err) => {
                console.error(err)
                return res.status(500).json({ error: err })
            })
    })
}

module.exports = {
    login,
    register
}
