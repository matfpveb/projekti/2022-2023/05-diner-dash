const mongoose = require('mongoose')
const Category = require('../models/categoryModel')

async function index(req, res, next) {
    Category.find({})
        .exec()
        .then((categories) => {
            res.status(200).json({
                categoryCount: categories.length,
                categories: categories
            })
        })
        .catch((error) => {
            res.status(500).json({
                error: error
            })
        })
}

async function show(req, res, next) {
    const id = req.params.id
    Category.findById(id)
        .then((cat) => {
            res.status(200).json({
                category: cat
            })
        })
        .catch((error) => {
            res.status(500).json({
                error: error
            })
        })
}

async function store(req, res, next) {
    const body = req.body
    const category = new Category({ _id: mongoose.Types.ObjectId(), ...body })
    category.save()
        .then((cat) => {
            return res.status(201).json({
                message: "Category created",
                category: cat
            })
        })
        .catch((error) => {
            return res.status(500).json({
                error: error
            })
        })
    
}

async function update(req, res, next) {
    const id = req.params.id
    const body = req.body

    Category.findByIdAndUpdate(id, body, ( err, cat ) => {
        if(err){
            res.status(500).json({
                error: err
            })
            return
        }

        res.status(200).json({
            message: "Category successfully updated",
            cat: cat 
        })
    })
}

async function deleteCategory(req, res, next) {
    const id = req.params.id
    Category.findById(id)
    .then((category) => {
        category.delete()
            .then( (result) => {
                res.status(200).json({
                    message: "Category successfully deleted",
                    category:result
                })
            })
            .catch( (error) => {
                res.status(500).json({
                    error: error
                })
            })
    })
    .catch( () => {
        res.status(404).json({
            error: "Category not found"
        })
    })
}

module.exports = {
    index,
    show,
    store,
    update,
    deleteCategory
}
