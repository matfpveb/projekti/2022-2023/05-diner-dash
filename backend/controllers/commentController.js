const mongoose = require('mongoose')
const Comment = require('../models/commentModel')

async function index(req, res, next) {
    Comment.find({})
        .exec()
        .then((comments) => {
            res.status(200).json({
                commentCount: comments.length,
                comments: comments
            })
        })
        .catch((error) => {
            res.status(500).json({
                error: error
            })
        })
}

async function show(req, res, next) {
    const id = req.params.id
    Comment.findById(id)
        .then((comm) => {
            res.status(200).json({
                comment: comm
            })
        })
        .catch((error) => {
            res.status(500).json({
                error: error
            })
        })
}

async function store(req, res, next) {
    const body = req.body
    const comment = new Comment({ _id: mongoose.Types.ObjectId(), ...body })
    
    comment.save()
        .then((comm) => {
            return res.status(201).json({
                message: "Comment created",
                comment: comm
            })
        })
        .catch((error) => {
            return res.status(500).json({
                error: error
            })
        })
    
}

async function update(req, res, next) {
    const id = req.params.id
    const body = req.body

    Comment.findByIdAndUpdate(id, body, ( err, comm ) => {
        if(err){
            res.status(500).json({
                error: err
            })
            return
        }

        res.status(200).json({
            message: "Comment successfully updated",
            comm: comm 
        })
    })
}

async function deleteComment(req, res, next) {
    const id = req.params.id
    Comment.findById(id)
    .then((comment) => {
        comment.delete()
            .then( (result) => {
                res.status(200).json({
                    message: "Comment successfully deleted",
                    comment :result
                })
            })
            .catch( (error) => {
                res.status(500).json({
                    error: error
                })
            })
    })
    .catch( () => {
        res.status(404).json({
            error: "Comment not found"
        })
    })
}

module.exports = {
    index,
    show,
    store,
    update,
    deleteComment
}
