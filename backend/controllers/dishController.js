const mongoose = require('mongoose')
const Dish = require('../models/dishModel')

async function index(req, res, next) {
    Dish.find({})
        .exec()
        .then((dishes) => {
            res.status(200).json({
                dishCount: dishes.length,
                dishes: dishes
            })
        })
        .catch((error) => {
            res.status(500).json({
                error: error
            })
        })
}

async function show(req, res, next) {
    const id = req.params.id
    Dish.findById(id)
        .then((dish) => {
            res.status(200).json({
                dish: dish
            })
        })
        .catch((error) => {
            res.status(500).json({
                error: error
            })
        })
}

async function store(req, res, next) {
    const body = req.body
    Dish.findOne({ name: body.name })
        .then(( dish ) => {
            if(dish) {
                res.status(500).json({
                    error: "Dish already exists",
                })
                return
            }

            dish = new Dish({_id: mongoose.Types.ObjectId(), ...body })
            dish.save()
                .then((dish) => {
                    return res.status(201).json({
                        message: "Dish created",
                        dish: dish
                    })
                })
                .catch((error) => {
                    return res.status(500).json({
                        error: error
                    })
                })
        })
}

async function update(req, res, next) {
    const id = req.params.id
    const body = req.body
    Dish.findByIdAndUpdate(id, body, ( err, dish ) => {
        if(err){
            res.status(500).json({
                error: err
            })
            return
        }

        res.status(200).json({
            message: "Dish successfully updated",
            dish: dish 
        })
    })
}

async function deleteDish(req, res, next) {
    const id = req.params.id
    Dish.findById(id)
        .then((dish) => {
            dish.delete()
                .then( (result) => {
                    res.status(200).json({
                        message: "Dish successfully deleted",
                        dish:result
                    })
                })
                .catch( (error) => {
                    res.status(500).json({
                        error: error
                    })
                })
        })
        .catch( () => {
            res.status(404).json({
                error: "Dish not found"
            })
        })
}

module.exports = {
    index,
    show,
    store,
    update,
    deleteDish
}
