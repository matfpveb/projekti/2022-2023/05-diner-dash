const path = require('path')


async function uploadImages(images) {
    images = Object.values(images)
    const paths = []
    for( let image of images ) {
        const pathName = uploadImage(image)
        if(pathName)
            paths.push(pathName)
    }
    return paths
}

async function uploadImage(image, imageName = null) {
    imageName = imageName ?? image.name
    let pathName = ""
    if(checkMimetype(image)) {
        pathName = path.resolve('public/' + imageName)
        await image.mv(pathName)
        return pathName
    }

    return false
}

function checkMimetype(image) {
   return /^image/.test(image.mimetype)
}

module.exports = {
    uploadImage,
    uploadImages
}