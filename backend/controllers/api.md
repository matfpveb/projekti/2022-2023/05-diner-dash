## API ENPOINTS

### /auth

| Request    | Endpoint             | Description            | Request Body                                     |  Response Params  |
| ---------- | -------------------- | ---------------------- | ------------------------------------------------ | ----------------- |
| POST       | /auth/login          | Log in a user          | username/email, password                         | message, token    |
| POST       | /auth/register       | Register a user        | username, email, password, first_name, last_name | message, token    |



** Endpoints below require Authorization token - set it to "Bearer " + token or just set a token in Authorization (Bearer Auth) section in Postman



### /users

| Request    | Endpoint             | Description            | Request Body                                                       |  Response Params                                                         |
| ---------- | -------------------- | ---------------------- | ------------------------------------------------------------------ | ------------------------------------------------------------------------ |
| GET        | /users               | Get all users          | /                                                                  | userCount, users [{ \_id, username, email, first_name, last_name,  is_admin, is_chef, is_delivery }]      |
| POST       | /users               | Create a user          | username, email, password, first_name, last_name  ( optional is_admin, is_chef, is_delivery)  | message, user { \_id, username, email, password, first_name, last_name, is_admin, is_chef, is_delivery } |
| GET        | /users/{userId}      | Get a user             | /                                                                  | user { \_id, username, email, password, first_name, last_name,  is_admin, is_chef, is_delivery }          |
| PUT        | /users/{userId}      | Update user            | (at least one of) username, email, password, first_name, last_name,  is_admin, is_chef, is_delivery | message, user { \_id, username, email, password, first_name, last_name } |
| DELETE     | /users/{userId}      | Delete a user          | /                                                                  | message, user { \_id, username, email, password, first_name, last_name,  is_admin, is_chef, is_delivery } |


### /categories

| Request    | Endpoint                 | Description            | Request Body               |  Response Params                                          |
| ---------- | ------------------------ | ---------------------- | -------------------------- | --------------------------------------------------------- |
| GET        | /categories              | Get all categories     | /                          | categoriesCount, categories [{ \_id, name, description }] |
| POST       | /categories              | Create a category      | name, description          | message, category { \_id, name, description }             |
| GET        | /categories/{categoryId} | Get a category         | /                          | category { \_id, name, description }                      |
| PUT        | /categories/{categoryId} | Update category        | (one of) name, description | message, category { \_id, name, description }             |
| DELETE     | /categories/{categoryId} | Delete a category      | /                          | message, category { \_id, name, description }             |

### /comments

| Request    | Endpoint              | Description      | Request Body                                |  Response Params                                          |
| ---------- | --------------------- | ---------------- | ------------------------------------------- | --------------------------------------------------------- |
| GET        | /comments             | Get all comments | /                                           | commentsCount, comments [{ \_id, user, comment, images }] |
| POST       | /comments             | Create a comment | user_id, comment, images(optional)          | message, comment { \_id, user, comment, images }          |
| GET        | /comments/{commentId} | Get a comment    | /                                           | comment { \_id, user, comment, images }                   |
| PUT        | /comments/{commentId} | Update comment   | (at least one of) user_id, comment, images  | message, comment { \_id, user, comment, images }          |
| DELETE     | /comments/{commentId} | Delete a comment | /                                           | message, comment { \_id, user, comment, images }          |

### /orders

| Request    | Endpoint          | Description    | Request Body                                     |  Response Params                                             |
| ---------- | ----------------- | -------------- | ------------------------------------------------ | ------------------------------------------------------------ |
| GET        | /orders           | Get all orders | /                                                | ordersCount, orders [{ \_id, first_name, last_name, email, phone, delivery_price, order_price, total_price, address, flat_number, floor, intercom_number, chef_message, user, items }]  |
| POST       | /orders           | Create a order | \_id, first_name, last_name, email, phone, delivery_price, order_price, total_price, address, flat_number, floor, intercom_number, chef_message, user, items                   | message, order { \_id, first_name, last_name, email, phone, delivery_price, order_price, total_price, address, flat_number, floor, intercom_number, chef_message, user, items }         |
| GET        | /orders/{orderId} | Get a order    | /                                                | order { \_id, price, address, user, items }                  |
| PUT        | /orders/{orderId} | Update order   | (at least one of) first_name, last_name, email, phone, delivery_price, order_price, total_price, address, flat_number, floor, intercom_number, chef_message, user, items | message, order { \_id, first_name, last_name, email, phone, delivery_price, order_price, total_price, address, flat_number, floor, intercom_number, chef_message, user, items }         |
| DELETE     | /orders/{orderId} | Delete a order | /                                                | message, order { \_id, \_id, \_id, first_name, last_name, email, phone, delivery_price, order_price, total_price, address, flat_number, floor, intercom_number, chef_message, user, items }         |

### /dishes

| Request    | Endpoint         | Description    | Request Body                         |  Response Params                                  |
| ---------- | ---------------- | -------------- | ------------------------------------ | ------------------------------------------------- |
| GET        | /dishes          | Get all dishes | /                                    | dishesCount, dishes [{ \_id, name, description }] |
| POST       | /dishes          | Create a dish  | name, description                    | message, dish { \_id, name, description }         |
| GET        | /dishes/{dishId} | Get a dish     | /                                    | dish { \_id, name, description }                  |
| PUT        | /dishes/{dishId} | Update dish    | (at least one of) name, description  | message, dish { \_id, name, description }         |
| DELETE     | /dishes/{dishId} | Delete a dish  | /                                    | message, dish { \_id, name, description }         |