const path = require('path');


module.exports.home = function (req, res, next) {
	res.sendFile(path.join(__dirname, '..', 'views', '/index.html'));
};

module.exports.jelovnik = function (req, res, next) {
	res.sendFile(path.join(__dirname, '..', 'views', '/jelovnik.html'));
};

module.exports.dostava = function (req, res, next) {
	res.sendFile(path.join(__dirname, '..', 'views', '/dostava.html'));
};

module.exports.kontakt = function (req, res, next) {
	res.sendFile(path.join(__dirname, '..', 'views', '/kontakt.html'));
};

module.exports.galerija = function (req, res, next) {
	res.sendFile(path.join(__dirname, '..', 'views', '/galerija.html'));
};

