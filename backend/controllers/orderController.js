const User = require('../models/userModel')
const Dish = require('../models/dishModel')
const mongoose = require('mongoose')
const Order = require('../models/orderModel')

async function index(req, res, next) {
    Order.find({})
        .exec()
        .then((orders) => {
            res.status(200).json({
                orderCount: orders.length,
                orders: orders
            })
        })
        .catch((error) => {
            res.status(500).json({
                error: error
            })
        })
}

async function show(req, res, next) {
    const id = req.params.id
    Order.findById(id)
        .populate('user')
        .populate('items')
        .exec((err, order) => {
            if(err) {
                res.status(500).json({ error: err})
                return
            }

            res.status(200).json({
                order: order
            })
        })
}

async function store(req, res, next) {
    const body = req.body
    const order = new Order({ _id: mongoose.Types.ObjectId(), ...body })
    order.save()
        .then((ord) => {
            return res.status(201).json({
                message: "Order created",
                order: ord
            })
        })
        .catch((error) => {
            return res.status(500).json({
                error: error
            })
        })
    
}

async function update(req, res, next) {
    const id = req.params.id
    const body = req.body

    Order.findByIdAndUpdate(id, body, ( err, ord ) => {
        if(err){
            res.status(500).json({
                error: err
            })
            return
        }

        res.status(200).json({
            message: "Order successfully updated",
            ord: ord 
        })
    })
}

async function deleteOrder(req, res, next) {
    const id = req.params.id
    Order.findById(id)
    .then((order) => {
        order.delete()
            .then( (result) => {
                res.status(200).json({
                    message: "Order successfully deleted",
                    order :result
                })
            })
            .catch( (error) => {
                res.status(500).json({
                    error: error
                })
            })
    })
    .catch( () => {
        res.status(404).json({
            error: "Order not found"
        })
    })
}

module.exports = {
    index,
    show,
    store,
    update,
    deleteOrder
}
