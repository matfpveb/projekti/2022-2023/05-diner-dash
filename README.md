# Project diner-dash

## Instrukcije za korišćenje

1\. Klonirati repozitorijum

```
git clone https://gitlab.com/matfpveb/projekti/2022-2023/05-diner-dash
```

2\. Potrebno je instalirati pakete

2\.1\. Potrebno je instalirati node >= 18.0 

2\.2\. Pozicionirajte se u backend folder repozitorijuma i pokrenite komandu 

```
npm install
```

2\.2\. Potrebno je instalirati mongodb bazu na sistemu ili koristiti mongodb cloud

```
https://www.mongodb.com/try/download/community
```

2\.2\.1\. (Opciono) Za koriscenje mongodb servera mozete se registrovati na sajtu i umesto dela:

```
mongoose.connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`, {...})
```

stavite nesto slicno ovome

```
mongoose.connect(`mongodb+srv://{USERNAME}:{PASSWORD}@{PUTANJA_DO_SERVERA}/diner_dash`, {...})
```

3\. Potrebno je postaviti env varijable koje se nalaze u .env.example fajlu nesto slicno ovome:

```
PORT=lokalni_port_za_pokretanje_servera
JWT_SECRET=neki_jwt_kod     # bilo sta je moguce staviti ovde
MONGODB_HOST=localhost      # ukoliko pokrecete aplikaciju na lokalu
MONGODB_PORT=27017          # standardni port mongodb baze
MONGODB_DATABASE=diner_dash # kako god da nazovete bazu 
MONGODB_CLUSTER_URL         # moze da se nalepi i link ka klasteru oblika mongodb+srv://{username}:{password}@{url_ka_klasteru}
BCRYPT_ROUNDS=10            # ovo mozete menjati ali sa povecanjem ce hashiranje passworda biti duze
``` 

4\. Pokrenite server komandom

```
npm serve
```

4\.1\. (Opciono) Preporucujemo koriscenje nodemon paketa kako bi izmene odmah bile vidljive

```
npm install -g nodemon
```

u backend direktorijumu pokrenite 

```
nodemon
```

## Instalirani paketi
### Backend

- *[bcrypt](https://www.npmjs.com/package/bcrypt)*
- *[body-parser](https://www.npmjs.com/package/body-parser)*
- *[express](https://www.npmjs.com/package/express)*
- *[jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)*
- *[mongoose](https://www.npmjs.com/package/mongoose)*
- *[dotenv](https://www.npmjs.com/package/dotenv)*
## Developers

- [Ivana Jordanov, 56/2015](https://gitlab.com/ivana.jordanov)
- [Nikola Panić, 284/2017](https://gitlab.com/mrpannic)
- [Nikola Novaković, 64/2018](https://gitlab.com/nikola_novakovic)
- [Petar Dinić, 90/2017](https://gitlab.com/ptrdnc)

## Video
- [Diner dash][def]

[def]: https://www.youtube.com/watch?v=9D5ruVhHToE
