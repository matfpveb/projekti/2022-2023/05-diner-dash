# Backend

[Uputstvo Za Koriscenje](https://gitlab.com/matfpveb/projekti/2022-2023/05-diner-dash/-/tree/frontend/backend)

# Frontend 

This is the frontend part of diner-dash application. 

# Building & running

This project was generated with [Angular CLI](https://github.com/angular/angular-cli).

To build and run this angular project on you pc you will need:

* OS: win32 x64
  * Angular CLI: 15.0.1 
  * Node: 18.12.1
  * Package Manager: npm 9.1.2

* OS: linux 
  * Angular CLI: 15.0.1 
  * Node: 18.12.1
  * Package Manager: npm 9.1.2

Additionaly, you will need to install bootstrap: `npm install bootstrap`

To build and start application run `ng serve` from this location. Then open `http://localhost:4200/` in your browser. 

