import { AuthService } from 'src/app/services/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pocetna',
  templateUrl: './pocetna.component.html',
  styleUrls: ['./pocetna.component.css']
})


export class PocetnaComponent implements OnInit {
  public currentUserType : string = 'gost';
  public picturePath : string = '';

  constructor(
    private authService: AuthService
  ) {
    let user = authService.getLoggedUser();
    this.currentUserType = user? user.userTag : 'gost';
  }

  ngOnInit(): void {
  }

}
