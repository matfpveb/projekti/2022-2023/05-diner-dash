import { Component , Input, OnInit} from '@angular/core';
import {Dish} from '../models/dish.model';
import { CartService } from '../services/cart.service';
import { DostavaComponent } from '../dostava/dostava.component';

@Component({
  selector: 'app-jelo',
  templateUrl: './jelo.component.html',
  styleUrls: ['./jelo.component.css']
})
export class JeloComponent  implements OnInit {

  @Input('jeloData')
  public jelo: Dish;


  constructor(public cartService : CartService) {
    this.jelo = new Dish("Abc", "Pera");
  }
  ngOnInit() {

  }
  public addToCart(jelo : Dish) {
    this.cartService.addDish(jelo);
  }

}
