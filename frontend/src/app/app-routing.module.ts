import { PocetnaComponent } from './pocetna/pocetna.component';
import { JelovnikComponent } from './jelovnik/jelovnik.component';
import { LoginFormComponent } from './logovanjePlusRegistracija/login-form/login-form.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DostavaComponent } from './dostava/dostava.component';
import { RegisterFormComponent } from './logovanjePlusRegistracija/register-form/register-form.component';

import { JelovnikCreatorComponent } from './jelovnik-creator/jelovnik-creator.component';


const routes: Routes = [
  {path: 'loginPage', component:LoginFormComponent},
  {path: 'registerPage', component:RegisterFormComponent},
  {path: 'dostava', component:DostavaComponent},
  {path: 'jelovnik', component:JelovnikComponent},
  {path: '', component:PocetnaComponent},
  {path: 'jelo', component:JelovnikCreatorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
