import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ReactiveFormsModule} from '@angular/forms';
import { LoginFormComponent } from './logovanjePlusRegistracija/login-form/login-form.component';
import { DostavaComponent } from './dostava/dostava.component';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { NavbarComponent } from './navbar/navbar.component';
import { JelovnikComponent } from './jelovnik/jelovnik.component';
import { PocetnaComponent } from './pocetna/pocetna.component'
import { JeloComponent } from './jelo/jelo.component';
import { JelovnikCreatorComponent } from './jelovnik-creator/jelovnik-creator.component';

import { HttpClientModule } from '@angular/common/http';
import { RegisterFormComponent } from './logovanjePlusRegistracija/register-form/register-form.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    DostavaComponent,
    HeaderComponent,
    BodyComponent,
    NavbarComponent,
    JelovnikComponent,
    PocetnaComponent,
    JeloComponent,
    JelovnikCreatorComponent,
    RegisterFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
