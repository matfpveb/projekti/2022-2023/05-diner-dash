import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/models/user.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public username: string = '';

  constructor(
    authService : AuthService
  ) {
    let user = authService.getLoggedUser();
    this.setUsername(user);
  }

  ngOnInit() {

  }

  public setUsername(user : User | null) {
    this.username = user ? user.first_name + " " + user.last_name : "";
  }

}
