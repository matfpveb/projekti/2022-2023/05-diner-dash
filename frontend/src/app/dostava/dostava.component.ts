import { SoundManagerService } from './../sound-manager.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CartService } from '../services/cart.service';
import { Dish } from '../models/dish.model';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-dostava',
  templateUrl: './dostava.component.html',
  styleUrls: ['./dostava.component.css']
})

export class DostavaComponent implements OnInit {
  public porudzbina: Dish[] = [];
  public forma: FormGroup;

  constructor(
    private cartService: CartService,
    private formBuilder: FormBuilder,
    private orderService : OrderService,
    private soundManagerService: SoundManagerService
    ) {
    this.porudzbina = this.cartService.getItems();

//     console.log("IN CART: ");
//     console.log(cartService.getItems());

    let cena_porudzbine = cartService.getCartPrice();
    let cena_dostave = 100;

    this.forma = this.formBuilder.group({
      first_name: ['', [Validators.required, Validators.pattern('[ a−zA−Z]+')]],
      last_name: ['',[Validators.required, Validators.pattern('[ a−zA−Z]+')]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.pattern('06[0-59][0−9]{6-7}')]],
      address: ['', [Validators.required, Validators.pattern('[ a−zA−Z0−9]+ [0−9]+')]],
      floor: ['', [Validators.required, Validators.pattern('[0-9]+')]],
      intercom_number: ['', [Validators.required, Validators.pattern('[1-9][0-9]*')]],
      dostavljacPor: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9 .]{,300}')]],
      chef_message: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9 .]{,300}')]],
      delivery_price: cena_dostave,
      order_price: cena_porudzbine,
      total_price: cena_dostave + cena_porudzbine,
    });
  }

  ngOnInit(): void {
  }

  public refresh() {
    this.forma.updateValueAndValidity();
  }

  public submitForm(data : string): void {
//     console.log("SUBMIT FORM : ");
//     console.log(data);

//     if (!this.forma.valid) {
//       this.soundManagerService.playButtonFail();
//       window.alert("INVALID FORM");
//       return;
//
//     }

    const orderData = this.forma.value;
//     console.log("ORDER DATA");
//     console.log(orderData);

    const items_ = []
    const cart = this.cartService.getItems();
    for (const dish of cart)
      items_.push(dish._id)

    orderData.items = items_;

//     console.log("ITEMS_ : ")
//     console.log(items_)

    const res = this.orderService.postOrder(orderData);
    //observable...

    this.soundManagerService.playButtonSuccess();
    this.cartService.clearCart();
    this.forma.reset();
    }
}

