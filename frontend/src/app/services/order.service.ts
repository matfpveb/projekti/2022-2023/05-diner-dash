import { Injectable } from '@angular/core';

import { Order } from 'src/app/models/order.model';

import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { JwtService } from './auth/jwt.service';

import { AuthService } from './auth/auth.service';


@Injectable({
  providedIn: 'root'
})

export class OrderService {

    private readonly ordersUrl = 'http://localhost:8080/api/orders';

    constructor(
      private http: HttpClient,
      private jwtService : JwtService,
      private authService : AuthService
    ) {

    }

    public getOrders() {
        return this.http.get(this.ordersUrl);
    }

    public getOrderById(id : string) {
        return this.http.get(this.ordersUrl + '/:' + id);
    }

    public postOrder(order : Order) {
//       console.log("POST ORDER");
//       console.log(order);

      let user_id = this.authService.getLoggedUser()!._id;

//       console.log("USER ID: ");
//       console.log(user_id);


        const body = {
            "first_name": order.first_name,
            "last_name": order.last_name,
            "email": order.email,
            "phone": order.phone,
            "address": order.address,
            "floor": order.floor,
            "intercom_number": order.intercom_number,
            "chef_message": order.chef_message,
            "delivery_price": order.delivery_price,
            "order_price": order.order_price,
            "total_price": order.total_price,
            "items" : order.items,
            "order_status" : 12,
            "user" : user_id
        };
        console.log("BODY")
        console.log(body);

         console.log("ITEMS: ")
      console.log(order.items);

        const headers = {
        'Content-Type': 'application/json',
        'Authorization': `${this.jwtService.getToken()}`
      }

      console.log(headers)

        return this.http.post(this.ordersUrl, body, { 'headers': headers }).subscribe((res: any) => console.log("POST!"));
    }

    public updateOrder(order : Order) {

      let user_id = this.authService.getLoggedUser()!._id;


        const body = {
            "first_name": order.first_name,
            "last_name": order.last_name,
            "email": order.email,
            "phone": order.phone,
            "address": order.address,
            "floor": order.floor,
            "intercom_number": order.intercom_number,
            "chef_message": order.chef_message,
            "delivery_price": order.delivery_price,
            "order_price": order.order_price,
            "total_price": order.total_price,
            "items" : order.items,
            "order_status" : 12,
            "user" : user_id
        };


        const headers = {
        'Content-Type': 'application/json',
        'Authorization': `${this.jwtService.getToken()}`
      }

        return this.http.put(this.ordersUrl, body, { 'headers': headers }).subscribe((res: any) => console.log("POST!"));
    }

    public deleteOrder(id : string) {
        return this.http.delete(this.ordersUrl + '/:' + id);
    }
}
