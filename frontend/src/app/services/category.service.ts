import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Category } from 'src/app/models/category.model';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

    private readonly categoriesUrl = 'http://localhost:8080/api/categories';

    constructor(private http: HttpClient) {
    }

    public getCategories() {
        return this.http.get(this.categoriesUrl);
    }

    public getCategoryById(id : string) {
        return this.http.get(this.categoriesUrl + '/:' + id);
    }

    public postCategory(category : String) {

        const body = {
            category : category
        };
        return this.http.post(this.categoriesUrl, body);
    }

    public updateCategory(category : String) {
        const body = {
            category : category
        };
        return this.http.put(this.categoriesUrl, body);
    }

    public deleteCategory(id : string) {
        return this.http.delete(this.categoriesUrl + '/:' + id);
    }
}
