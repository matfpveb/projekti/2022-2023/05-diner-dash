import { Injectable } from '@angular/core';
import { Dish } from '../models/dish.model';

@Injectable({
    providedIn: 'root'
})
export class CartService {

    private dishes : Dish[] = [];

    constructor() { }

    public addDish(dish : Dish) {
      console.log("ADDING DISH");
      console.log(dish);


        this.dishes.push(dish);
        console.log("DISHES IN CART: ");
      console.log(this.dishes);
    }

    public getItems() {
        return this.dishes;
    }

    public clearCart() {
        this.dishes = [];
    }

    public getCartPrice() {
        let res : number= 0;
        for (const dish of this.dishes)
            res+=dish.price;
        return res;
    }
}
