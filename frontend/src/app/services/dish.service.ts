import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomOptions } from '../CustomOptions';

import { JwtService } from './auth/jwt.service';

import { Dish } from '../models/dish.model';

@Injectable({
  providedIn: 'root'
})

export class DishService {

    private readonly dishUrl = 'http://localhost:8080/api/dishes';
    private options = new CustomOptions()
    constructor(
      private http: HttpClient,
      private jwtService : JwtService
    ) {

        this.getDishes();
      }

    public getDishes(): Observable<Dish[]> {
        return this.http.get<Dish[]>(this.dishUrl, this.options.getOptions() );
    }


    public getDishById(id : string) {
        return this.http.get(this.dishUrl + '/' + id, this.options.getOptions())
    }

    public postDish(dish : Dish) {

        const body = {
            "name" : dish.name,
            "description" : dish.description,
            "price" : dish.price
        }

        const headers = {
        'Content-Type': 'application/json',
        'Authorization': `${this.jwtService.getToken()}`
      }
        return this.http.post(this.dishUrl, body, { 'headers': headers }).subscribe((res: any) => console.log("POST!"));
    }

    public updateDish(dish : Dish) {
        const body = {
            dish : dish
        }
        return this.http.put(this.dishUrl, body, this.options.getOptions());
    }

    public deleteDish(id : string) {
        return this.http.delete(this.dishUrl + '/' + id, this.options.getOptions());
    }
}
