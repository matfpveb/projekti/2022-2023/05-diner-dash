import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { User } from 'src/app/models/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})

export class UserService {

    private readonly userUrl = 'http://localhost:8080/api/users';
    constructor(
      private http: HttpClient
    ) {

    }

    public getUsers() {
        return this.http.get(this.userUrl);
    }

    public getUserById(id : string) {
        return this.http.get(this.userUrl + '/:' + id);
    }

    public postUser(user : String) {
        const body = {
            user : user
        };
        return this.http.post(this.userUrl, body);
    }

    public updateUser(user : String) {
        const body = {
            user : user
        };
        return this.http.put(this.userUrl, body);
    }

    public deleteUser(id : string) {
        return this.http.delete(this.userUrl + '/' + id);
    }

}
