import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user.model';

@Injectable({
    providedIn: "root"
})
export class JwtService {
    private static readonly TOKENID: string = "JWT_TOKEN";

    constructor() {}

    public getToken(): string | null {
        //console.log("LOCAL STORAGE");
        //console.log(localStorage);
        const token: string | null = localStorage.getItem(JwtService.TOKENID);
        if (!token) {
            return null;
        }
        return token;
    }

    public getDataFromToken(): User | null{
        const token = this.getToken();
        if (!token) {
            return null;
        }

        const payloadString = token.split(".")[1];
        const userDataJSON = window.atob(payloadString);
        const payload: User = JSON.parse(userDataJSON);

        //console.log(userDataJSON);
        console.log("from gettoken")
        console.log(payload);
        return payload;
    }

    public setToken(jwt: string): void {
        console.log("SETTING TOKEN : " + jwt);
        localStorage.setItem(JwtService.TOKENID, "Bearer " + jwt);
    }

    public removeToken(): void {
        localStorage.removeItem(JwtService.TOKENID);
    }
}
