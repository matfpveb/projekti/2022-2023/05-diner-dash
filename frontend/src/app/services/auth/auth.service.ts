import { Injectable } from '@angular/core';
import { JwtService } from './jwt.service';
import { HttpClient } from '@angular/common/http';

import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public readonly url = "http://localhost:8080/auth/"
  private readonly token = 'JWT_TOKEN'

  constructor(
    private jwtService: JwtService,
    private http: HttpClient
  ) {}

   public login(email: String, password: String) {
    this.http.post(this.url + "login", {"username": email, "password": password})
      .subscribe((res: any) => this.jwtService.setToken(res.token))
  }

  public register(data: User) {
    console.log("AUTH");
    console.log(data);
    console.log(typeof(data));

    let body = {
      "email" : data.email,
      "password" : data.password,

      "first_name" : data.first_name,
      "last_name" : data.last_name,

      "username" : data.username
    }

    this.http.post(this.url + "register", body)
      .subscribe((res: any) => this.jwtService.setToken(res.token))
  }

  public logout() {
    this.jwtService.removeToken()
  }

  public getLoggedUser() {
    return this.jwtService.getDataFromToken()
  }

  public isAdmin() : boolean {
    let user = this.jwtService.getDataFromToken();
    if (user === null)
      return false;
    return user?.userTag === "admin"
  }

  public isCook() : boolean {
    let user = this.jwtService.getDataFromToken();
    if (user === null)
      return false;
    return user.is_chef
  }

  public isKlijent() : boolean {
    let user = this.jwtService.getDataFromToken();
    if (user === null)
      return false;
    return user?.userTag === "klijent"
  }

  public isGuest() : boolean {
    let user = this.jwtService.getDataFromToken();
    if (user === null)
      return true;
    return false
  }

  public isLoggedIn(): boolean {
    return this.jwtService.getToken() != null;
  }

}
