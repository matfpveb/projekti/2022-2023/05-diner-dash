import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Comment } from 'src/app/models/comment.model';


@Injectable({
  providedIn: 'root'
})
export class CommentService {
    private readonly commentsUrl = 'http://localhost:8080/api/comments';

    constructor(private http: HttpClient) {

    }

    public getComments() {
        return this.http.get(this.commentsUrl);
    }

    public getCommentById(id : string) {
        return this.http.get(this.commentsUrl + '/:' + id);
    }

    public postComment(comment : String) {

        const body = {
            comment : comment
        };
        return this.http.post(this.commentsUrl, body);
    }

    public updateComment(comment : String) {

        const body = {
            comment : comment
        };
        return this.http.put(this.commentsUrl, body);
    }

    public deleteComment(id : string) {
        return this.http.delete(this.commentsUrl + '/:' + id);
    }
}
