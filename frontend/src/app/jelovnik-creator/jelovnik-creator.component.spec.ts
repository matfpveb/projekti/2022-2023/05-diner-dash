import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JelovnikCreatorComponent } from './jelovnik-creator.component';

describe('JelovnikCreatorComponent', () => {
  let component: JelovnikCreatorComponent;
  let fixture: ComponentFixture<JelovnikCreatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JelovnikCreatorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JelovnikCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
