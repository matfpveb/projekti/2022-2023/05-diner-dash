import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DishService } from '../services/dish.service';
import { Dish } from '../models/dish.model';

@Component({
  selector: 'app-jelovnik-creator',
  templateUrl: './jelovnik-creator.component.html',
  styleUrls: ['./jelovnik-creator.component.css']
})
export class JelovnikCreatorComponent implements OnInit{
  public forma: FormGroup;

  ngOnInit(): void {
  }

  constructor (
    private formBuilder: FormBuilder,
    private dishService : DishService
  ) {
    this.forma = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern('[ a−zA−Z]+')]],
      price: ['', [Validators.required, Validators.pattern('[1-9][0-9]*')]],
      description: ['', [Validators.required, Validators.pattern('[a-zA-Z 0-9.!@#$%^&*()_+-=-/]*')]]
    });
  }

  public submitForm(data: Dish) : void {
    console.log(data);
//     if (!this.forma.valid) {
//     window.alert('Not valid!');
//     return;
//     }

     // Contact server here...

    this.dishService.postDish(data);

    // this.porudzbina = this.korpaService.clearCart();
    this.forma.reset();
  }

}
