import { AuthService } from './../../services/auth/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms'
import { Router } from '@angular/router';
import { SoundManagerService } from 'src/app/sound-manager.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent implements OnInit, OnDestroy {
  public loginForma: FormGroup;
  public isLoggedIn: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private soundManagerService: SoundManagerService
  ) {
      this.loginForma = this.formBuilder.group({
        username: [''],
        password: ['']
      })

      this.isLoggedIn = this.authService.isLoggedIn();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  public submitForm(data : String): void {
    console.log(data);

//     if (!this.loginForma.valid)
//     {
//       this.soundManagerService.playButtonFail();
//       return;
//     }

    this.soundManagerService.playButtonSuccess();

    const loginData = this.loginForma.value;
    this.authService.login(loginData.username, loginData.password);

    this.loginForma.reset();
    this.router.navigateByUrl("/");
  }

  logout(): void {
    this.soundManagerService.playButtonSuccess();
    this.authService.logout();
    this.router.navigateByUrl("/");
  }

}
