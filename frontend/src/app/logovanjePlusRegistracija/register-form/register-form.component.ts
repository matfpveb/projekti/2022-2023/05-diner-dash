import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SoundManagerService } from 'src/app/sound-manager.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})

export class RegisterFormComponent implements OnInit {
  public registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private soundManagerService: SoundManagerService
  ) {
      this.registerForm = this.formBuilder.group({
        first_name: ['', [Validators.required, Validators.pattern('[ a−zA−Z]+')]],
        last_name: ['',[Validators.required, Validators.pattern('[ a−zA−Z]+')]],
        username: ['',[Validators.required, Validators.pattern('[ a−zA−Z]+')]],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9/*-+!@#$%^&*()_]{8,}')]],
        sifraPotvrda: ['']
      })
  }

  ngOnInit(): void {
  }

  public submitForm(data : String): void {
    console.log(data);

//     if (!this.registerForm.valid)
//     {
//       console.log("INVALID!");
//       this.soundManagerService.playButtonFail();
//       return;
//     }

    const loginData = this.registerForm.value;

    // check if passwords matches
//     const sifra1: string = loginData.sifra;
//     const sifra2: string = loginData.sifraPotvrda;
//     const valid: boolean = (sifra1 === sifra2);
//     if (!valid) {
//       todo: obavesti korisnika da je greska u sifri
//       this.soundManagerService.playButtonFail();
//       return;
//     }

    console.log("STIGAO");

    console.log(loginData);
    this.authService.register(loginData);

    // if register fails we ar enot logged in
    if (this.authService.isLoggedIn())
    {
      this.registerForm.reset();
      this.soundManagerService.playButtonSuccess();
    }

    this.soundManagerService.playButtonFail();
  }
}
