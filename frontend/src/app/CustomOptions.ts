import { HttpHeaders } from "@angular/common/http";
import { JwtService } from "./services/auth/jwt.service";

export class CustomOptions {
  private jwtService
  private headers
  constructor() {
    this.jwtService = new JwtService()
    this.headers = {
      "authorization" : this.jwtService.getToken() ?? ""
    }
  }

  public getHeaders() {
    return new HttpHeaders(this.headers)
  }

  public getOptions() {
    return { headers: this.getHeaders() }
  }
}
