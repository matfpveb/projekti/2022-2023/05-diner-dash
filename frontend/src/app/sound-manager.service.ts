import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class SoundManagerService {
  private mainAudio = new Audio();
  private effectSound = new Audio();

  constructor() {
  }

   playBackgroundMusicOnRepeat(){
    this.mainAudio.src = "../../assets/backgroundSound.mp3";
    this.mainAudio.loop = true;
    this.mainAudio.load();
    this.mainAudio.play();
   }

   playButtonFail() {
    this.effectSound.src = "../assets/orderNotAvailableSound.mp3";
    this.effectSound.load();
    this.effectSound.play();
   }

   playButtonSuccess() {
    this.effectSound.src = "../assets/buttonSoundEffect.mp3";
    this.effectSound.load();
    this.effectSound.play();
   }
}
