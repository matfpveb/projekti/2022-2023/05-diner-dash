import { Component, OnInit, OnDestroy, AfterViewChecked } from '@angular/core';

import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit, OnDestroy, AfterViewChecked  {
  private menu = document.querySelector("#c-circle-nav");
  private toggle = document.querySelector("#c-circle-nav__toggle");
  private mask = document.querySelector("#senka");
  private activeClass = "is-active";

  constructor(
    public authService : AuthService
  ) {}

  maskClick(){
    this.deactivateMenu();
    console.log("maska klik!");
  }

  menuClick() {
    console.log("[debug] navBar klik!");
    this.toggle?.classList.contains(this.activeClass) ? this.deactivateMenu() : this.activateMenu();
    console.log("IS COOK! : ")
    console.log(this.authService.isCook());
  };

  activateMenu(): void {
    this.menu?.classList.add(this.activeClass);
    this.toggle?.classList.add(this.activeClass);
    this.mask?.classList.add(this.activeClass);
  }

  deactivateMenu() {
    this.menu?.classList.remove(this.activeClass);
    this.toggle?.classList.remove(this.activeClass);
    this.mask?.classList.remove(this.activeClass);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  ngAfterViewChecked(): void {
    this.menu = document.querySelector("#c-circle-nav");
    this.toggle = document.querySelector("#c-circle-nav__toggle");
    this.mask = document.querySelector("#senka");

    this.mask?.addEventListener("click", this.deactivateMenu);
    // bug: why doen't it work?!
  }
}


