  import { Component, OnInit } from '@angular/core';
import { Observable , tap, of} from 'rxjs';
import { Dish } from '../models/dish.model';
import { DishService } from '../services/dish.service'
import { AuthService } from '../services/auth/auth.service';
@Component({
  selector: 'app-jelovnik',
  templateUrl: './jelovnik.component.html',
  styleUrls: ['./jelovnik.component.css']
})
export class JelovnikComponent  implements OnInit{
  public dishes: Observable<Dish[]> = new Observable<Dish[]>;

  //constructor(private dishService : DishServiceService) {
  constructor (private dishService: DishService, public authService : AuthService) {

  }

  ngOnInit(): void {


    this.dishService.getDishes().subscribe( (dishes : any) => {
      this.dishes = of(dishes['dishes']);
    } );
  }
}
