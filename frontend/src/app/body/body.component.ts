import { SoundManagerService } from './../sound-manager.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})

export class BodyComponent {

  constructor(
    private soundManagerService : SoundManagerService
  ) {
    this.soundManagerService.playBackgroundMusicOnRepeat();
  }

}
