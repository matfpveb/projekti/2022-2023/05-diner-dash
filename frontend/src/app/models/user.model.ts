export class User {
  constructor(
    public _id : string = "1",
    public password : string = "pass",
    public email : string = "email@email.com",
    public first_name : string = "name",
    public last_name : string = "surname",
    public image_path: string = "",
    public userTag: string = "",
    public username: string = "",
    public is_chef : boolean = false
  ){}
}
