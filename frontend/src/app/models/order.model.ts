export class Order {
  constructor(
    public id : string = "1",
    public user : string = "12",
    public address : string = "Ulica..",
    public items : string[] = [],
    public first_name: string = "Abc",
    public last_name: string = "Abc",
    public email: string = "abc@gmail.com",
    public phone : string = "0600005555",
    public total_price : number = 1000,
    public floor: number = 0,
    public intercom_number: number = 0,
    public chef_message : string = "",
    public delivery_price: number = 0,
    public order_price: number = 0,
    public order_status: number = 0,

  ){}

}
